#!/bin/bash
sudo apt-get update && sudo apt-get upgrade -y
sudo apt install -y mysql-server
sudo apt-get install -y software-properties-common debconf-utils
sudo apt-get update --fix-missing
sudo apt-get install -y openjdk-8-jdk
sudo systemctl enable mysql
sudo systemctl start mysql

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list

sudo apt-get update && sudo apt-get install -y elasticsearch
sudo sed -i -r 's/^#?\s?network\.host:\s?[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/network.host: 0.0.0.0/g' /etc/elasticsearch/elasticsearch.yml
sudo echo "discovery.type: single-node" >> /etc/elasticsearch/elasticsearch.yml
sudo sed -i -r 's/-Xms[0-9]+(m|g)/-Xms512m/g' /etc/elasticsearch/jvm.options
sudo sed -i -r 's/-Xmx[0-9]+(m|g)/-Xmx512m/g' /etc/elasticsearch/jvm.options
sudo /bin/systemctl daemon-reload 
sudo /bin/systemctl enable elasticsearch.service
sudo /bin/systemctl start elasticsearch

sudo apt-get update && sudo apt-get install -y kibana
sudo sed -i -r 's/^#?\s?server\.host:\s?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}|"localhost")/server.host: 0.0.0.0/g' /etc/kibana/kibana.yml
sudo /bin/systemctl daemon-reload 
sudo /bin/systemctl enable kibana.service
sudo /bin/systemctl start kibana

sudo mysql -e "create user konakart identified by 'konakart2020';create database konakart; grant all on konakart.* to konakart; flush privileges;"
curl -L -O https://www.konakart.com/kits/9.2.0.0/KonaKart-9.2.0.0-Linux-Install-64
sudo chmod +x KonaKart-9.2.0.0-Linux-Install-64
./KonaKart-9.2.0.0-Linux-Install-64 -S -DDatabaseDriver com.mysql.jdbc.Driver -DLoadDB 1 -DJavaJRE /usr/lib/jvm/java-8-openjdk-amd64 -DDatabaseType mysql -DDatabaseUrl jdbc:mysql://localhost:3306/konakart -DDatabaseUsername konakart -DDatabasePassword konakart2020